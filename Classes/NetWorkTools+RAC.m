//
//  NetworkTools+RAC.m
//  GuoKongYiTiHua
//
//  Created by 邱中卫 on 2018/6/24.
//  Copyright © 2018年 邱中卫. All rights reserved.
//

#import "NetWorkTools+RAC.h"

@implementation NetWorkTools (RAC)

- (RACSignal *)request:(NSString *)URLString parameters:(id)parameters methodType:(RequestMethodType)methodType{
    
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        
        [NetWorkTools request:URLString parameters:parameters methodType:methodType success:^(NSObject *response) {
            NSDictionary *dict = (NSDictionary *)response;
            [subscriber sendNext:dict];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            NSError *err = [NetWorkTools setErrorInfor:error];
            [subscriber sendNext:@{@"message":(err.userInfo[NSLocalizedDescriptionKey]) }];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (RACSignal *)PostImageUrl:(NSString*)url
                     params:(NSDictionary*)urlparameters
                 imageScale:(CGFloat)imageScale
                  imageSize:(CGSize)imageSize
                      image:(UIImage*)image{
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        //接收类型不一致请替换一致text/html或别的
        [NetWorkTools PostImageUrl:url
                            params:urlparameters
                             image:image
                        imageScale:imageScale
                         imageSize:imageSize
                          progress:^(NSProgress *progress){
                              [subscriber sendNext:progress];
                              [subscriber sendCompleted];
        } success:^(NSObject *response) {
            NSDictionary *dict = (NSDictionary *)response;
            [subscriber sendNext:dict];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            NSError *err = [NetWorkTools setErrorInfor:error];
            [subscriber sendNext:@{@"message":(err.userInfo[NSLocalizedDescriptionKey]) }];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

- (RACSignal *)PostImageUrl:(NSString *)url
                     params:(NSDictionary *)urlparameters
                     images:(NSArray<UIImage *> *)images
                 imageScale:(CGFloat)imageScale
                  imageSize:(CGSize)imageSize{
    return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        //接收类型不一致请替换一致text/html或别的
        
        [NetWorkTools PostImageUrl:url
                            params:urlparameters
                            images:images
                         mageScale:imageScale
                         imageSize:imageSize
                          progress:^(NSProgress *progress) {
                              [subscriber sendNext:progress];
                              [subscriber sendCompleted];
        } success:^(NSObject *response) {
            NSDictionary *dict = (NSDictionary *)response;
            [subscriber sendNext:dict];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            NSError *err = [NetWorkTools setErrorInfor:error];
        
            [subscriber sendNext:@{@"message":(err.userInfo[NSLocalizedDescriptionKey]) }];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}
- (RACSignal *)downloadWithURL:(NSString *)url
                       fileDir:(NSString *)fileDir {
     return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
         [NetWorkTools downloadWithURL:url
                               fileDir:fileDir
                              progress:^(NSProgress *progress) {
                                  [subscriber sendNext:progress];
                                  [subscriber sendCompleted];
         } success:^(NSString *filePath) {
             [subscriber sendNext:filePath];
             [subscriber sendCompleted];
         } failure:^(NSError *error) {
             NSError *err = [NetWorkTools setErrorInfor:error];
             [subscriber sendNext:@{@"message":(err.userInfo[NSLocalizedDescriptionKey]) }];
             [subscriber sendCompleted];
         }];
         return nil;
     }];
}
@end
