//
//  loginUserInfo.m
//  SongYuFlowerStore
//
//  Created by 邱中卫 on 2016/11/27.
//  Copyright © 2016年 邱中卫. All rights reserved.
//

#import "NetWorkTools.h"
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>


#ifdef DEBUG
#define PPLog(...) printf("[%s] %s [第%d行]: %s\n", __TIME__ ,__PRETTY_FUNCTION__ ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String])
#else
#define PPLog(...)
#endif

#define ValidDict(f) (f!=nil && [f isKindOfClass:[NSDictionary class]])
#define kCustomErrorDomain @"com.yk.ios"

typedef enum {
    eCustomErrorCodeFailure = 0
} eCustomErrorCode;

@implementation NetWorkTools: NSObject

static BOOL _isOpenLog;   // 是否已开启日志打印
static BOOL _isOpenAES;   // 是否已开启加密传输
static NSMutableArray *_allSessionTask;
static AFHTTPSessionManager *_sessionManager;

+ (instancetype)sharedTools{
    
    static NetWorkTools * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#pragma mark - 初始化AFHTTPSessionManager相关属性
/**
 开始监测网络状态
 */
+ (void)load {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}
/**
 *  所有的HTTP请求共享一个AFHTTPSessionManager
 *  原理参考地址:http://www.jianshu.com/p/5969bbb4af9f
 */
+ (void)initialize {
    _sessionManager = [AFHTTPSessionManager manager];
    // 设置请求的超时时间
    _sessionManager.requestSerializer.timeoutInterval = 30.f;
    
    // 设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    //    _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*",@"text/encode", nil];
    // 打开状态栏的等待菊花
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    //开启加密模式
    [self openAES];
}
//采用^运算直接换算成结果.不会进入字符串常量区
#define STRING_ENCRYPT_KEY 0xAC
static NSString * AES_KEY(){
    unsigned char key[] = {
        (STRING_ENCRYPT_KEY ^ 'I'),
        (STRING_ENCRYPT_KEY ^ 'U'),
        (STRING_ENCRYPT_KEY ^ 'I'),
        (STRING_ENCRYPT_KEY ^ 'D'),
        (STRING_ENCRYPT_KEY ^ 'I'),
        (STRING_ENCRYPT_KEY ^ 'S'),
        (STRING_ENCRYPT_KEY ^ 'T'),
        (STRING_ENCRYPT_KEY ^ 'P'),
        (STRING_ENCRYPT_KEY ^ '#'),
        (STRING_ENCRYPT_KEY ^ '\0')
    };
    unsigned char * p = key;
    while (((*p) ^=  STRING_ENCRYPT_KEY) != '\0') p++;
    
    return [NSString stringWithUTF8String:(const char *)key];
}
#pragma mark - ——————— 加密 Header ————————
+ (void)encodeHeader{
 
}
+ (NSString*)JSONString {
    NSError* error = nil;
    id jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                  options:NSJSONWritingPrettyPrinted error:&error];
    if (error != nil) return nil;
    
    NSString *jsonString=[[NSString alloc] initWithData:jsonData
                                               encoding:NSUTF8StringEncoding];
    
    return jsonString;
    
}
#pragma mark - ——————— 加密 header 和 Body ————————
+ (void)encodeParameters:(id)param{
    //加密 Header
//    [self encodeHeader];
//    
//    //参数不为空 且 已经开启加密
//    if (ValidDict(param) && _isOpenAES) {
//        [_sessionManager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
//            NSString *contentStr = [parameters jsonStringEncoded];
//            NSString *AESStr = [[EncryptionToolsClass sharedEncryptionTools]encryptString:@"" keyString:contentStr iv:nil];;
//            return AESStr;
//        }];
//    }
}
#pragma mark - 开始监听网络
+ (void)networkStatusWithBlock:(PPNetworkStatus)networkStatus {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                    status ? networkStatus(AFNetworkReachabilityStatusUnknown) : nil;
                    if (_isOpenLog) PPLog(@"未知网络");
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                    status ? networkStatus(AFNetworkReachabilityStatusNotReachable) : nil;
                    if (_isOpenLog) PPLog(@"无网络");
                    break;
                case AFNetworkReachabilityStatusReachableViaWWAN:
                    status ? networkStatus(AFNetworkReachabilityStatusReachableViaWWAN) : nil;
                    if (_isOpenLog) PPLog(@"手机自带网络");
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    status ? networkStatus(AFNetworkReachabilityStatusReachableViaWiFi) : nil;
                    if (_isOpenLog) PPLog(@"WIFI");
                    break;
            }
        }];
    });
}

+ (BOOL)isNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+ (BOOL)isWWANNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachableViaWWAN;
}

+ (BOOL)isWiFiNetwork {
    return [AFNetworkReachabilityManager sharedManager].reachableViaWiFi;
}

+ (void)openLog {
    _isOpenLog = YES;
}

+ (void)closeLog {
    _isOpenLog = NO;
}

#pragma mark - ——————— 开关加密 ————————
+ (void)openAES {
    _isOpenAES = YES;
    [_sessionManager.requestSerializer setValue:@"text/encode" forHTTPHeaderField:@"Content-Type"];
    _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
}

+ (void)closeAES {
    _isOpenAES = NO;
    [_sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
}
/**
 存储着所有的请求task数组
 */
+ (NSMutableArray *)allSessionTask {
    if (!_allSessionTask) {
        _allSessionTask = [[NSMutableArray alloc] init];
    }
    return _allSessionTask;
}

+ (void)cancelAllRequest {
    // 锁操作
    @synchronized(self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            [task cancel];
        }];
        [[self allSessionTask] removeAllObjects];
    }
}

+ (void)cancelRequestWithURL:(NSString *)URL {
    if (!URL) { return; }
    @synchronized (self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task.currentRequest.URL.absoluteString hasPrefix:URL]) {
                [task cancel];
                [[self allSessionTask] removeObject:task];
                *stop = YES;
            }
        }];
    }
}
+ (NSURLSessionTask *)request:(NSString *)URLString
                   parameters:(id)parameters
                   methodType:(RequestMethodType)methodType
                      success:(void(^)(id response))success
                      failure:(void(^)(NSError *error))failure {

    return [self request:URLString
              parameters:parameters
              methodType:methodType
           responseCache:nil
                 success:success
                 failure:failure];
}
+ (NSURLSessionTask *)request:(NSString *)URLString
                   parameters:(id)parameters
                   methodType:(RequestMethodType)methodType
                responseCache:(void(^)(id response))responseCache
                      success:(void(^)(id response))success
                      failure:(void(^)(NSError *error))failure {
    NSURLSessionTask *sessionTask;
    //读取缓存
//    responseCache!=nil ? responseCache([NetWorkCache httpCacheForURL:URLString parameters:parameters]) : nil;
    //加密 header body
    [self encodeParameters:parameters];
    if (methodType == RequestMethodTypePost) {
        
        sessionTask = [_sessionManager POST:URLString parameters:parameters
                                   progress:nil
                                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                        NSDictionary *dict = (NSDictionary *)responseObject;
                                        success(dict);
                                        //对数据进行异步缓存
//                                        responseCache!=nil ? [NetWorkCache setHttpCache:responseObject URL:URLString parameters:parameters] : nil;
                                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                        
                                        NSError *err = [self setErrorInfor:error];
                                        failure(err);
                                    }];
        
    } else if (methodType == RequestMethodTypeGet) {
        sessionTask = [_sessionManager GET:URLString parameters:parameters
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       NSLog(@"%@",responseObject);
                                       NSDictionary *dict = (NSDictionary *)responseObject;
                                       success ? success(dict) : nil;
                                       //对数据进行异步缓存
//                                       responseCache!=nil ? [NetWorkCache setHttpCache:responseObject URL:URLString parameters:parameters] : nil;
                                       
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       NSLog(@"%@",error);
                                       NSError *err = [self setErrorInfor:error];
                                       failure ? failure (err) :nil;
                                   }];
    }
    // 添加最新的sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil ;
    return sessionTask;
    
}
+ (NSURLSessionTask *)PostImageUrl:(NSString*)url
                            params:(NSDictionary*)urlparameters
                             image:(UIImage*)image
                        imageScale:(CGFloat)imageScale
                         imageSize:(CGSize)imageSize
                          progress:(void(^)(NSProgress *progress))progress
                           success:(void(^)(id response))success
                           failure:(void(^)(NSError *error))failure {

        //接收类型不一致请替换一致text/html或别的
        
        NSURLSessionDataTask *task = [_sessionManager POST:url parameters:urlparameters constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
   
            UIImage*image_up = [self imageByResizeToSize:imageSize image:image];
            NSData *imageData = UIImageJPEGRepresentation(image_up, imageScale ?: 1.f);

            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            formatter.dateFormat =@"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
            
            //上传的参数(上传图片，以文件流的格式)
            [formData appendPartWithFileData:imageData
                                        name:@"file"
                                    fileName:fileName
                                    mimeType:@"image/jpeg"];
            
        } progress:^(NSProgress *_Nonnull uploadProgress) {
            //打印下上传进度
            progress ? progress(uploadProgress) : nil;
        } success:^(NSURLSessionDataTask *_Nonnull task,id _Nullable responseObject) {
            //上传成功
            NSLog(@"%@",responseObject);
            NSDictionary *dict = (NSDictionary *)responseObject;
            success ? success( dict) : nil;
            
        } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
            NSLog(@"%@",error);
            NSError *err = [self setErrorInfor:error];
            failure ? failure (err) :nil;
        }];
        
        [task resume];
        task ? [[self allSessionTask]addObject:task] :nil;
        return task;
}

+ (NSURLSessionTask *)PostImageUrl:(NSString*)url
                            params:(NSDictionary*)urlparameters
                            images:(NSArray<UIImage *> *)images
                         mageScale:(CGFloat)imageScale
                         imageSize:(CGSize)imageSize
                          progress:(void(^)(NSProgress *progress))progress
                           success:(void(^)(id response))success
                           failure:(void(^)(NSError *error))failure {
        //接收类型不一致请替换一致text/html或别的
        dispatch_group_t group = dispatch_group_create();
        dispatch_semaphore_t semphore = dispatch_semaphore_create(5);//最大并发线程
        __block BOOL uploadFailure = NO;
        NSMutableDictionary *responseDic = [NSMutableDictionary dictionary];
        if (images.count > 0) {
            
            for (int i = 0 ; i< images.count ; i++) {
                dispatch_group_enter(group);
                
                NSURLSessionDataTask *task = [_sessionManager POST:url parameters:urlparameters constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
                    UIImage *image = images[i];
                    UIImage *image_up = [self imageByResizeToSize:imageSize image:image];
                    NSData *imageData = UIImageJPEGRepresentation(image_up, imageScale ?: 1.f);
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    formatter.dateFormat =@"yyyyMMddHHmmss";
                    NSString *str = [formatter stringFromDate:[NSDate date]];
                    NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
                    
                    //上传的参数(上传图片，以文件流的格式)
                    [formData appendPartWithFileData:imageData
                                                name:@"file"
                                            fileName:fileName
                                            mimeType:@"image/jpeg"];
                    
                } progress:^(NSProgress *_Nonnull uploadProgress) {
                    //打印下上传进度
                } success:^(NSURLSessionDataTask *_Nonnull task,id _Nullable responseObject) {
                    //上传成功
                    NSLog(@"%@",responseObject);
                    
                    NSDictionary *dict = (NSDictionary *)responseObject;
                    [responseDic setObject:dict forKey:[NSString stringWithFormat:@"%d",i]];
                    
                    dispatch_group_leave(group);
                    dispatch_semaphore_signal(semphore);
                    
                } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
                    NSLog(@"%@",error);
                    uploadFailure = YES;
                    
                    dispatch_group_leave(group);
                    dispatch_semaphore_signal(semphore);
                }];
                
                [task resume];
            }
            
            dispatch_group_notify( group, dispatch_get_main_queue(), ^{
                if (uploadFailure == YES) {
                    success(responseDic);
                }else {
                    success(responseDic);
                }
            });
            
            dispatch_semaphore_wait(semphore, DISPATCH_TIME_FOREVER);
        }
        return nil;
}
+ (__kindof NSURLSessionTask *)downloadWithURL:(NSString *)URL
                                       fileDir:(NSString *)fileDir
                                      progress:(void(^)(NSProgress *progress))progress
                                       success:(void(^)(NSString *filePath))success
                                       failure:(void(^)(NSError *error))failure {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL]];
    __block NSURLSessionDownloadTask *downloadTask = [_sessionManager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        //下载进度
        dispatch_sync(dispatch_get_main_queue(), ^{
            progress ? progress(downloadProgress) : nil;
        });
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //拼接缓存目录
        NSString *downloadDir = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:fileDir ? fileDir : @"Download"];
        //打开文件管理器
        NSFileManager *fileManager = [NSFileManager defaultManager];
        //创建Download目录
        [fileManager createDirectoryAtPath:downloadDir withIntermediateDirectories:YES attributes:nil error:nil];
        //拼接文件路径
        NSString *filePath = [downloadDir stringByAppendingPathComponent:response.suggestedFilename];
        //返回文件位置的URL路径
        return [NSURL fileURLWithPath:filePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        [[self allSessionTask] removeObject:downloadTask];
        if(failure && error) {failure(error) ; return ;};
        success ? success(filePath.absoluteString /** NSURL->NSString*/) : nil;
        
    }];
    //开始下载
    [downloadTask resume];
    // 添加sessionTask到数组
    downloadTask ? [[self allSessionTask] addObject:downloadTask] : nil ;
    return downloadTask;
}
+ (NSError*)setErrorInfor:(NSError*)error {
    NSDictionary *userInfo;
    
    if (error.code == NSURLErrorCancelled) {
        // 若取消则不发送任何消息
        userInfo = [NSDictionary dictionaryWithObject:@"取消访问"
                                               forKey:NSLocalizedDescriptionKey];
        
    } else if (error.code == NSURLErrorTimedOut) {
        
        userInfo = [NSDictionary dictionaryWithObject:@"网络超时"
                                               forKey:NSLocalizedDescriptionKey];
        
    } else if (error.code == NSURLErrorNotConnectedToInternet){
        
        userInfo = [NSDictionary dictionaryWithObject:@"无网络连接"
                    
                                               forKey:NSLocalizedDescriptionKey];
    }else if (error.code == NSURLErrorNetworkConnectionLost){
        
        userInfo = [NSDictionary dictionaryWithObject:@"网络连接异常"
                                               forKey:NSLocalizedDescriptionKey];
    }else{
        
        userInfo = [NSDictionary dictionaryWithObject:@"网络错误"
                                               forKey:NSLocalizedDescriptionKey];
        
    }
    NSError *err = [[NSError alloc]initWithDomain:kCustomErrorDomain
                                             code:error.code
                                         userInfo:userInfo];
    return err;
}
+ (UIImage *)imageByResizeToSize:(CGSize)size image:(UIImage*)image{
    if (size.width <= 0 || size.height <= 0) return nil;
    UIGraphicsBeginImageContextWithOptions(size, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - 重置AFHTTPSessionManager相关属性

+ (void)setAFHTTPSessionManagerProperty:(void (^)(AFHTTPSessionManager *))sessionManager {
    sessionManager ? sessionManager(_sessionManager) : nil;
}

+ (void)setRequestSerializer:(HttpResponseType)requestSerializer {
    _sessionManager.requestSerializer = requestSerializer==HttpResponseType_Common ? [AFHTTPRequestSerializer serializer] : [AFJSONRequestSerializer serializer];
}

+ (void)setResponseSerializer:(HttpResponseType)responseSerializer {
    _sessionManager.responseSerializer = responseSerializer==HttpResponseType_Common ? [AFHTTPResponseSerializer serializer] : [AFJSONResponseSerializer serializer];
}

+ (void)setRequestTimeoutInterval:(NSTimeInterval)time {
    _sessionManager.requestSerializer.timeoutInterval = time;
}

+ (void)setValue:(NSString *)value forHTTPHeaderField:(NSString *)field {
    [_sessionManager.requestSerializer setValue:value forHTTPHeaderField:field];
}

+ (void)openNetworkActivityIndicator:(BOOL)open {
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:open];
}

+ (void)setSecurityPolicyWithCerPath:(NSString *)cerPath validatesDomainName:(BOOL)validatesDomainName {
    NSData *cerData = [NSData dataWithContentsOfFile:cerPath];
    // 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    // 如果需要验证自建证书(无效证书)，需要设置为YES
    securityPolicy.allowInvalidCertificates = YES;
    // 是否需要验证域名，默认为YES;
    securityPolicy.validatesDomainName = validatesDomainName;
    securityPolicy.pinnedCertificates = [[NSSet alloc] initWithObjects:cerData, nil];
    
    [_sessionManager setSecurityPolicy:securityPolicy];
}

@end
#pragma mark - NSDictionary,NSArray的分类
/*
 ************************************************************************************
 *新建NSDictionary与NSArray的分类, 控制台打印json数据中的中文
 ************************************************************************************
 */

#ifdef DEBUG
@implementation NSArray (PP)

- (NSString *)descriptionWithLocale:(id)locale {
    NSMutableString *strM = [NSMutableString stringWithString:@"(\n"];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [strM appendFormat:@"\t%@,\n", obj];
    }];
    [strM appendString:@")"];
    
    return strM;
}

@end

@implementation NSDictionary (PP)

- (NSString *)descriptionWithLocale:(id)locale {
    NSMutableString *strM = [NSMutableString stringWithString:@"{\n"];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [strM appendFormat:@"\t%@ = %@;\n", key, obj];
    }];
    
    [strM appendString:@"}\n"];
    
    return strM;
}
@end
#endif

