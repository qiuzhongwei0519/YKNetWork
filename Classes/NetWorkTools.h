//
//  loginUserInfo.m
//  SongYuFlowerStore
//
//  Created by 邱中卫 on 2016/11/27.
//  Copyright © 2016年 邱中卫. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef enum {
    HttpResponseType_Common,/** 设置请求数据为二进制格式*/
    HttpResponseType_Json,/** 设置请求数据为JSON格式*/
    HttpResponseType_XML
}HttpResponseType;

typedef NS_ENUM(NSInteger, RequestMethodType){
    RequestMethodTypePost = 1,
    RequestMethodTypeGet = 2
};
/** 网络状态的Block*/
typedef void(^PPNetworkStatus)(AFNetworkReachabilityStatus status);

@interface NetWorkTools : NSObject

+ (instancetype)sharedTools;

/**
 有网YES, 无网:NO
 */
+ (BOOL)isNetwork;

/**
 手机网络:YES, 反之:NO
 */
+ (BOOL)isWWANNetwork;

/**
 WiFi网络:YES, 反之:NO
 */
+ (BOOL)isWiFiNetwork;

/**
 取消所有HTTP请求
 */
+ (void)cancelAllRequest;

/**
 实时获取网络状态,通过Block回调实时获取(此方法可多次调用)
 */
+ (void)networkStatusWithBlock:(PPNetworkStatus)networkStatus;

/**
 取消指定URL的HTTP请求
 */
+ (void)cancelRequestWithURL:(NSString *)URL;

/**
 开启日志打印 (Debug级别)
 */
+ (void)openLog;

/**
 关闭日志打印,默认关闭
 */
+ (void)closeLog;

/**
 开启加密
 */
//+ (void)openAES;

/**
 关闭加密  关闭后要记得开启 不然影响其他接口
 */
//+ (void)closeAES;

+ (NSError*)setErrorInfor:(NSError*)error;

/**
 不带缓存的http请求

 @param URLString url
 @param parameters 参数
 @param methodType 请求类型
 @param success 成功返回block
 @param failure 失败返回block
 @return 返回的对象可取消请求,调用cancel方法
 */
+ (NSURLSessionTask *)request:(NSString *)URLString
                   parameters:(id)parameters
                   methodType:(RequestMethodType)methodType
                      success:(void(^)(id response))success
                      failure:(void(^)(NSError *error))failure;
/**
 带缓存的http请求
 
 @param URLString url
 @param parameters 参数
 @param methodType 请求类型
 @param responseCache 缓存数据
 @param success 成功返回block
 @param failure 失败返回block
 @return 返回的对象可取消请求,调用cancel方法
 */
+ (NSURLSessionTask *)request:(NSString *)URLString
                   parameters:(id)parameters
                   methodType:(RequestMethodType)methodType
                responseCache:(void(^)(id response))responseCache
                      success:(void(^)(id response))success
                      failure:(void(^)(NSError *error))failure;

/**
  http请求单图片

 @param url url
 @param urlparameters 参数
 @param image 图片
 @param imageScale 压缩参数
 @param progress 进度条
 @param imageSize 图片压缩后的尺寸
 @param success 成功返回block
 @param failure 失败返回block
 @return 返回的对象可取消请求,调用cancel方法
 */

+ (__kindof NSURLSessionTask *)PostImageUrl:(NSString*)url
                                     params:(NSDictionary*)urlparameters
                                      image:(UIImage*)image
                                 imageScale:(CGFloat)imageScale
                                  imageSize:(CGSize)imageSize
                                   progress:(void(^)(NSProgress *progress))progress
                                    success:(void(^)(id response))success
                                    failure:(void(^)(NSError *error))failure;
/**
 多图片上传，使用多线程并发
 
 @param url url
 @param urlparameters 参数
 @param images 图片数组
 @param imageScale 压缩参数
 @param imageSize 图片压缩后的尺寸
 @param success 成功返回block
 @param failure 失败返回block
 @return 返回的对象可取消请求,调用cancel方法
 */

+ (__kindof NSURLSessionTask *)PostImageUrl:(NSString*)url
                                     params:(NSDictionary*)urlparameters
                                     images:(NSArray<UIImage *> *)images
                                  mageScale:(CGFloat)imageScale
                                  imageSize:(CGSize)imageSize
                                   progress:(void(^)(NSProgress *progress))progress
                                    success:(void(^)(id response))success
                                    failure:(void(^)(NSError *error))failure;
/**
 *  下载文件
 *
 *  @param URL      请求地址
 *  @param fileDir  文件存储目录(默认存储目录为Download)
 *  @param progress 文件下载的进度信息
 *  @param success  下载成功的回调(回调参数filePath:文件的路径)
 *  @param failure  下载失败的回调
 *
 *  @return 返回NSURLSessionDownloadTask实例，可用于暂停继续，暂停调用suspend方法，开始下载调用resume方法
 */
+ (__kindof NSURLSessionTask *)downloadWithURL:(NSString *)URL
                                       fileDir:(NSString *)fileDir
                                      progress:(void(^)(NSProgress *progress))progress
                                       success:(void(^)(NSString *filePath))success
                                       failure:(void(^)(NSError *error))failure;

#pragma mark - 设置AFHTTPSessionManager相关属性
#pragma mark 注意: 因为全局只有一个AFHTTPSessionManager实例,所以以下设置方式全局生效
/**
 在开发中,如果以下的设置方式不满足项目的需求,就调用此方法获取AFHTTPSessionManager实例进行自定义设置
 (注意: 调用此方法时在要导入AFNetworking.h头文件,否则可能会报找不到AFHTTPSessionManager的❌)
 @param sessionManager AFHTTPSessionManager的实例
 */
+ (void)setAFHTTPSessionManagerProperty:(void(^)(AFHTTPSessionManager *sessionManager))sessionManager;

/**
 *  设置网络请求参数的格式:默认为二进制格式
 *
 *  @param requestSerializer PPRequestSerializerJSON(JSON格式),PPRequestSerializerHTTP(二进制格式),
 */
+ (void)setRequestSerializer:(HttpResponseType)requestSerializer;

/**
 *  设置服务器响应数据格式:默认为JSON格式
 *
 *  @param responseSerializer PPResponseSerializerJSON(JSON格式),PPResponseSerializerHTTP(二进制格式)
 */
+ (void)setResponseSerializer:(HttpResponseType)responseSerializer;

/**
 *  设置请求超时时间:默认为30S
 *
 *  @param time 时长
 */
+ (void)setRequestTimeoutInterval:(NSTimeInterval)time;

/**
 *  设置请求头
 */
+ (void)setValue:(NSString *)value forHTTPHeaderField:(NSString *)field;

/**
 *  是否打开网络状态转圈菊花:默认打开
 *
 *  @param open YES(打开), NO(关闭)
 */
+ (void)openNetworkActivityIndicator:(BOOL)open;

/**
 配置自建证书的Https请求, 参考链接: http://blog.csdn.net/syg90178aw/article/details/52839103
 
 @param cerPath 自建Https证书的路径
 @param validatesDomainName 是否需要验证域名，默认为YES. 如果证书的域名与请求的域名不一致，需设置为NO; 即服务器使用其他可信任机构颁发
 的证书，也可以建立连接，这个非常危险, 建议打开.validatesDomainName=NO, 主要用于这种情况:客户端请求的是子域名, 而证书上的是另外
 一个域名。因为SSL证书上的域名是独立的,假如证书上注册的域名是www.google.com, 那么mail.google.com是无法验证通过的.
 */
+ (void)setSecurityPolicyWithCerPath:(NSString *)cerPath validatesDomainName:(BOOL)validatesDomainName;
@end
