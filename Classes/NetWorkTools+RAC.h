//
//  NetworkTools+RAC.h
//  GuoKongYiTiHua
//
//  Created by 邱中卫 on 2018/6/24.
//  Copyright © 2018年 邱中卫. All rights reserved.
//

#import "NetWorkTools.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface NetWorkTools (RAC)

- (RACSignal *)request:(NSString *)URLString
            parameters:(id)parameters
            methodType:(RequestMethodType)methodType;

- (RACSignal *)PostImageUrl:(NSString*)url
                     params:(NSDictionary*)urlparameters
                 imageScale:(CGFloat)imageScale
                  imageSize:(CGSize)imageSize
                      image:(UIImage*)image;

- (RACSignal *)PostImageUrl:(NSString*)url
                     params:(NSDictionary*)urlparameters
                 imageScale:(CGFloat)imageScale
                  imageSize:(CGSize)imageSize
                     images:(NSArray<UIImage *> *)images;

- (RACSignal *)downloadWithURL:(NSString *)url
                       fileDir:(NSString *)fileDir;
@end
