#
# Be sure to run `pod lib lint YKNetWork.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YKNetWork'
  s.version          = '0.0.2'
  s.summary          = 'YKNetWork.'
  s.description      = 'YKNetWork...'
  s.homepage         = 'https://gitee.com/qiuzhongwei0519'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'qiuzhongwei0519' => '512866393@qq.com' }
  s.source           = { :git => 'https://gitee.com/qiuzhongwei0519/YKNetWork.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.source_files = 'Classes', 'Classes/**/*.{h,m}'
  s.dependency 'AFNetworking'
  s.dependency 'ReactiveObjC'

end
