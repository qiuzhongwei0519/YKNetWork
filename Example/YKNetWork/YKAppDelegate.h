//
//  YKAppDelegate.h
//  YKNetWork
//
//  Created by scottlove0519 on 06/25/2018.
//  Copyright (c) 2018 scottlove0519. All rights reserved.
//

@import UIKit;

@interface YKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
