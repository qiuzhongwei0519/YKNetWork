//
//  main.m
//  YKNetWork
//
//  Created by scottlove0519 on 06/25/2018.
//  Copyright (c) 2018 scottlove0519. All rights reserved.
//

@import UIKit;
#import "YKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YKAppDelegate class]));
    }
}
